# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fr.romainvigier.MetadataCleaner package.
# Sergio Varela <sergitroll9@gmail.com>, 2021, 2022.
# Romain Vigier <romain@romainvigier.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: fr.romainvigier.MetadataCleaner\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-11 18:41+0000\n"
"PO-Revision-Date: 2022-07-17 17:16+0000\n"
"Last-Translator: Sergio Varela <sergitroll9@gmail.com>\n"
"Language-Team: Basque <https://hosted.weblate.org/projects/metadata-cleaner/"
"application/eu/>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.14-dev\n"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:6
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:7
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:9
#: application/data/ui/Window.ui:107 application/metadatacleaner/app.py:38
msgid "Metadata Cleaner"
msgstr "Metadatuen garbitzailea"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:8
msgid "Clean metadata from your files"
msgstr "Garbitu metadatuak zure fitxategietatik"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:15
msgid "Metadata;Remover;Cleaner;"
msgstr "Metadatuak;Ezabatzailea;Garbitzailea;"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:10
msgid "View and clean metadata in files"
msgstr "Metadatuak artxiboetan ikusi eta garbitu"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:26
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"Fitxategi bateko metadatuek asko esan dezakete pertsona bati buruz. Kamerek "
"argazki bat noiz eta non atera zen eta zein kamera erabili zen adierazten "
"dute. Aplikazio ofimatikoek egileari eta enpresari buruzko informazioa "
"gehitzen diete automatikoki dokumentuei eta kalkulu-orriei. Informazio "
"delikatua da, eta baliteke jakinarazi nahi ez izatea."

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:27
msgid ""
"This tool allows you to view metadata in your files and to get rid of it, as "
"much as possible."
msgstr ""
"Tresna honi esker, metadatuak artxiboetan ikus ditzakezu, eta ahal den "
"neurrian kendu."

#: application/data/gtk/help-overlay.ui:15
msgid "Files"
msgstr "Fitxategiak"

#: application/data/gtk/help-overlay.ui:18
#: application/data/ui/AddFilesButton.ui:49
msgid "Add files"
msgstr "Gehitu fitxategiak"

#: application/data/gtk/help-overlay.ui:24
#: application/data/ui/AddFilesButton.ui:56
msgid "Add folders"
msgstr "Gehitu karpetak"

#: application/data/gtk/help-overlay.ui:30
msgid "Clean metadata"
msgstr "Metadatuak garbitu"

#: application/data/gtk/help-overlay.ui:36
msgid "Clear all files from window"
msgstr "Leihoko fitxategi guztiak garbitu"

#: application/data/gtk/help-overlay.ui:44
msgid "General"
msgstr "Orokorra"

#: application/data/gtk/help-overlay.ui:47
msgid "New window"
msgstr "Leiho berria"

#: application/data/gtk/help-overlay.ui:53
msgid "Close window"
msgstr "Itxi leihoa"

#: application/data/gtk/help-overlay.ui:59
msgid "Quit"
msgstr "Irten"

#: application/data/gtk/help-overlay.ui:65
msgid "Keyboard shortcuts"
msgstr "Teklatuko lasterbideak"

#: application/data/gtk/help-overlay.ui:71
msgid "Help"
msgstr "Laguntza"

#: application/data/ui/AddFilesButton.ui:21
msgid "_Add Files"
msgstr "_Fitxategiak gehitu"

#: application/data/ui/AddFilesButton.ui:33
msgid "Add _Folders"
msgstr "Gehitu _karpetak"

#: application/data/ui/CleanMetadataButton.ui:9
msgid "_Clean"
msgstr "_Garbitu"

#: application/data/ui/CleaningWarningDialog.ui:9
msgid "Make sure you backed up your files!"
msgstr "Ziurtatu zure fitxategien babeskopia bat egiten duzula!"

#: application/data/ui/CleaningWarningDialog.ui:10
msgid "Once the files are cleaned, there's no going back."
msgstr "Artxiboak garbitu ondoren, ez dago atzera bueltarik."

#: application/data/ui/CleaningWarningDialog.ui:17
msgid "Don't tell me again"
msgstr "Ez erakutsi berriro mezu hau"

#: application/data/ui/CleaningWarningDialog.ui:25
#: application/data/ui/StatusIndicator.ui:37
msgid "Cancel"
msgstr "Ezeztatu"

#: application/data/ui/CleaningWarningDialog.ui:30
msgid "Clean"
msgstr "Garbitu"

#: application/data/ui/EmptyView.ui:13
msgid "Clean Your Traces"
msgstr "Garbitu zure oinatzak"

#: application/data/ui/EmptyView.ui:39
msgid "Learn more about metadata and the cleaning process limitations"
msgstr ""
"Lortu informazio gehiago metadatuei eta garbiketa-prozesuaren mugei buruz"

#: application/data/ui/FileRow.ui:11
msgid "Remove file from list"
msgstr "Kendu fitxategia zerrendatik"

#: application/data/ui/FileRow.ui:128
msgid "Warning"
msgstr "Ohartarazpena"

#: application/data/ui/FileRow.ui:146
msgid "Error"
msgstr "Errorea"

#: application/data/ui/FileRow.ui:186
msgid "Cleaned"
msgstr "Garbituta"

#: application/data/ui/MenuButton.ui:10
msgid "_New Window"
msgstr "_Leiho berria"

#: application/data/ui/MenuButton.ui:14
msgid "_Clear Window"
msgstr "_Garbitu leihoa"

#: application/data/ui/MenuButton.ui:20
msgid "_Help"
msgstr "_Laguntza"

#: application/data/ui/MenuButton.ui:25
msgid "_Keyboard Shortcuts"
msgstr "_Teklatu-lasterbideak"

#: application/data/ui/MenuButton.ui:29
msgid "_About Metadata Cleaner"
msgstr "_ Metadatuen Garbitzaileari buruz"

#: application/data/ui/MenuButton.ui:38
#, fuzzy
#| msgid "Main menu"
msgid "Main Menu"
msgstr "Menu nagusia"

#: application/data/ui/SettingsButton.ui:9
msgid "Cleaning settings"
msgstr "Garbiketa ezarpenak"

#: application/data/ui/SettingsButton.ui:27
msgid "Lightweight Cleaning"
msgstr "Garbiketa arina"

#: application/data/ui/SettingsButton.ui:32
msgid "Learn more about the lightweight cleaning"
msgstr "Garbiketa arinari buruzko informazio gehiago"

#: application/data/ui/Window.ui:80
msgid "Details"
msgstr "Xehetasunak"

#: application/data/ui/Window.ui:86
msgid "Close"
msgstr "Itxi"

#. Translators: Replace `translator-credits` by your name, and optionally your email address between angle brackets (Example: `Name <mail@example.org>`) or your website (Example: `Name https://example.org/`). If names are already present, do not remove them and add yours on a new line.
#: application/data/ui/Window.ui:125
msgid "translator-credits"
msgstr "Sergio Varela <sergitroll9@gmail.com>"

#: application/data/ui/Window.ui:128
msgid "Choose files to clean"
msgstr "Aukeratu garbitu nahi dituzun fitxategiak"

#: application/data/ui/Window.ui:138
msgid "Choose folders to clean"
msgstr "Aukeratu garbitu nahi dituzun karpetak"

#: application/metadatacleaner/modules/file.py:244
msgid "An error occured during the cleaning."
msgstr ""

#: application/metadatacleaner/modules/file.py:247
msgid "Something bad happened during the cleaning, cleaned file not found"
msgstr ""
"Zerbait txarra gertatu zen garbiketan, garbitu gabeko fitxategia ez da "
"aurkitu"

#: application/metadatacleaner/ui/detailsview.py:58
msgid "The File Has Been Cleaned"
msgstr "Fitxategia garbitu egin da"

#: application/metadatacleaner/ui/detailsview.py:60
msgid ""
"Known metadata have been removed, however the cleaning process has some "
"limitations."
msgstr ""
"Ezagutzen diren metadatuak ezabatu dira, baina garbiketa-prozesuak muga "
"batzuk ditu."

#: application/metadatacleaner/ui/detailsview.py:63
msgid "Learn more"
msgstr "Gehiago jakin"

#: application/metadatacleaner/ui/detailsview.py:76
msgid "Unable to Read the File"
msgstr "Ezin da fitxategia irakurri"

#: application/metadatacleaner/ui/detailsview.py:77
msgid "File Type not Supported"
msgstr "Fitxategi mota hau ez da onartzen"

#: application/metadatacleaner/ui/detailsview.py:79
msgid "Unable to Check for Metadata"
msgstr "Ezin dira metadatuak egiaztatu"

#: application/metadatacleaner/ui/detailsview.py:80
msgid "No Known Metadata"
msgstr "Metadatu ezagunik ez"

#: application/metadatacleaner/ui/detailsview.py:82
msgid "Unable to Remove Metadata"
msgstr "Ezin dira metadatuak ezabatu"

#: application/metadatacleaner/ui/detailsview.py:86
msgid "The file will be cleaned anyway to be sure."
msgstr "Nolanahi ere, artxiboa garbituko da seguru egoteko."

#: application/metadatacleaner/ui/filechooserdialog.py:24
msgid "All supported files"
msgstr "Onartutako fitxategi guztiak"

#: application/metadatacleaner/ui/folderchooserdialog.py:22
msgid "Add files from subfolders"
msgstr "Gehitu fitxategiak azpikarpetetatik"

#: application/metadatacleaner/ui/statusindicator.py:37
msgid "Processing file {}/{}"
msgstr "{} / {} Fitxategia prozesatzen"

#: application/metadatacleaner/ui/statusindicator.py:39
msgid "Cleaning file {}/{}"
msgstr "{} / {} Fitxategia garbitzen"

#: application/metadatacleaner/ui/statusindicator.py:79
#, python-format
msgid "%i file cleaned."
msgid_plural "%i files cleaned."
msgstr[0] ""
msgstr[1] ""

#: application/metadatacleaner/ui/statusindicator.py:84
#, python-format
msgid "%i error occured."
msgid_plural "%i errors occured."
msgstr[0] ""
msgstr[1] ""

#: application/metadatacleaner/ui/window.py:112
msgid "Libraries"
msgstr ""

#~ msgid "Clean without warning"
#~ msgstr "Garbitu ohartarazi gabe"

#~ msgid "Clean the files without showing the warning dialog"
#~ msgstr "Artxiboak garbitu ohartarazpen-elkarrizketa erakutsi gabe"

#~ msgid "Lightweight cleaning"
#~ msgstr "Garbiketa arina"

#~ msgid "Don't make destructive changes to files but may leave some metadata"
#~ msgstr ""
#~ "Ez egin aldaketa suntsitzailerik fitxategietan, baina metadatu batzuk utz "
#~ "ditzake"

#~ msgid "Window width"
#~ msgstr "Leihoaren zabalera"

#~ msgid "Saved width of the window"
#~ msgstr "Leihoaren zabalera gordeta"

#~ msgid "Window height"
#~ msgstr "Leihoaren altuera"

#~ msgid "Saved height of the window"
#~ msgstr "Leihoaren altuera gordeta"

#~ msgid "Updated translations"
#~ msgstr "Itzulpen eguneratuak"

#~ msgid "Improved user interface"
#~ msgstr "Erabiltzaile-interfaze hobetua"

#~ msgid "New translations"
#~ msgstr "Itzulpen berriak"

#~ msgid "Bug fixes"
#~ msgstr "Erroreen zuzenketa"

#~ msgid "New button to add folders"
#~ msgstr "Karpetak gehitzeko botoi berria"

#~ msgid "Improved adaptive user interface"
#~ msgstr "Erabiltzaile-interfaze egokigarri hobetua"

#~ msgid "New help pages"
#~ msgstr "Laguntza-orri berriak"

#~ msgid "One-click cleaning, no need to save after cleaning"
#~ msgstr "Garbitu klik bakarrarekin, garbitu ondoren gorde beharrik gabe"

#~ msgid "Persistent lightweight cleaning option"
#~ msgstr "Garbiketa arin iraunkorra egiteko aukera"

#~ msgid "Files with uppercase extension can now be added"
#~ msgstr "Orain, letra larriz luzatutako fitxategiak gehitu daitezke"

#~ msgid "About"
#~ msgstr "Honi buruz"

#~ msgid "Chat on Matrix"
#~ msgstr "Txateatu Matrix-en"

#~ msgid "View the code on GitLab"
#~ msgstr "Ikusi kodea GitLab-en"

#~ msgid "Translate on Weblate"
#~ msgstr "Itzuli Weblate-n"

#~ msgid "Support us on Liberapay"
#~ msgstr "Lagundu iezaguzu Liberapay-n"

#~ msgid "Credits"
#~ msgstr "Kredituak"

#~ msgid "Code"
#~ msgstr "Kodea"

#~ msgid "Artwork"
#~ msgstr "Artelanak"

#~ msgid "Documentation"
#~ msgstr "Dokumentazioa"

#~ msgid "Translation"
#~ msgstr "Itzulpena"

#~ msgid ""
#~ "This program uses <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> to "
#~ "parse and clean the metadata. Show them some love!"
#~ msgstr ""
#~ "Programa honek <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> "
#~ "erabiltzen du metadatuak aztertzeko. Erakutsi maitasun pixka bat!"

#~ msgid ""
#~ "The source code of this program is released under the terms of the <a "
#~ "href=\"https://www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 or later</"
#~ "a>. The original artwork and translations are released under the terms of "
#~ "the <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA "
#~ "4.0</a>."
#~ msgstr ""
#~ "Programa honen iturburu kodea <a href=\"https://www.gnu.org/licenses/"
#~ "gpl-3.0.html\"> GNU GPL 3.0 edo berriagoen </a> baldintzen arabera "
#~ "kaleratzen da. Jatorrizko artelanak eta itzulpenak <a href=\"https://"
#~ "creativecommons.org/licenses/by-sa/4.0/\"> CC BY-SA 4.0 </a> "
#~ "baldintzapean argitaratzen dira."

#~ msgid "Adding files…"
#~ msgstr "Fitxategiak gehitzen…"

#~ msgid "Croatian translation (contributed by Milo Ivir)"
#~ msgstr "Kroazierazko itzulpena (Milo Ivir-ek emana)"

#~ msgid "Portuguese (Brazil) translation (contributed by Gustavo Costa)"
#~ msgstr "Portugeserako itzulpena (Brasil) (Gustavo Costak emana)"

#~ msgid "New in v1.0.4:"
#~ msgstr "Berria 1.0.4 bertsioan:"

#~ msgid "Turkish translation (contributed by Oğuz Ersen)"
#~ msgstr "Turkiarrarentzako itzulpena (Oğuz Ersen-ek emana)"

#~ msgid "New in v1.0.2:"
#~ msgstr "Berria 1.0.2 bertsioan:"

#~ msgid "Spanish translation (contributed by Óscar Fernández Díaz)"
#~ msgstr "Gaztelaniazko itzulpena (Óscar Fernández Díaz-ek emana)"

#~ msgid "Swedish translation (contributed by Åke Engelbrektson)"
#~ msgstr "Suedierazko itzulpena (Åke Engelbrektson-ek emana)"

#~ msgid "New in v1.0.1:"
#~ msgstr "Berria 1.0.1 bertsioan:"

#~ msgid "German translation (contributed by lux)"
#~ msgstr "Alemanezko itzulpena (lux-ek emana)"
