# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fr.romainvigier.MetadataCleaner package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: fr.romainvigier.MetadataCleaner\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-03-11 18:32+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:6
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:7
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:9
#: application/data/ui/Window.ui:107 application/metadatacleaner/app.py:38
msgid "Metadata Cleaner"
msgstr ""

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:8
msgid "Clean metadata from your files"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:15
msgid "Metadata;Remover;Cleaner;"
msgstr ""

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:10
msgid "View and clean metadata in files"
msgstr ""

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:17
msgid "Romain Vigier"
msgstr ""

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:27
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:28
msgid ""
"This tool allows you to view metadata in your files and to get rid of it, as "
"much as possible."
msgstr ""

#: application/data/gtk/help-overlay.ui:15
msgid "Files"
msgstr ""

#: application/data/gtk/help-overlay.ui:18
#: application/data/ui/AddFilesButton.ui:49
msgid "Add files"
msgstr ""

#: application/data/gtk/help-overlay.ui:24
#: application/data/ui/AddFilesButton.ui:56
msgid "Add folders"
msgstr ""

#: application/data/gtk/help-overlay.ui:30
msgid "Clean metadata"
msgstr ""

#: application/data/gtk/help-overlay.ui:36
msgid "Clear all files from window"
msgstr ""

#: application/data/gtk/help-overlay.ui:44
msgid "General"
msgstr ""

#: application/data/gtk/help-overlay.ui:47
msgid "New window"
msgstr ""

#: application/data/gtk/help-overlay.ui:53
msgid "Close window"
msgstr ""

#: application/data/gtk/help-overlay.ui:59
msgid "Quit"
msgstr ""

#: application/data/gtk/help-overlay.ui:65
msgid "Keyboard shortcuts"
msgstr ""

#: application/data/gtk/help-overlay.ui:71
msgid "Help"
msgstr ""

#: application/data/ui/AddFilesButton.ui:21
msgid "_Add Files"
msgstr ""

#: application/data/ui/AddFilesButton.ui:33
msgid "Add _Folders"
msgstr ""

#: application/data/ui/CleanMetadataButton.ui:9
msgid "_Clean"
msgstr ""

#: application/data/ui/CleaningWarningDialog.ui:9
msgid "Make sure you backed up your files!"
msgstr ""

#: application/data/ui/CleaningWarningDialog.ui:10
msgid "Once the files are cleaned, there's no going back."
msgstr ""

#: application/data/ui/CleaningWarningDialog.ui:17
msgid "Don't tell me again"
msgstr ""

#: application/data/ui/CleaningWarningDialog.ui:25
#: application/data/ui/StatusIndicator.ui:37
msgid "Cancel"
msgstr ""

#: application/data/ui/CleaningWarningDialog.ui:30
msgid "Clean"
msgstr ""

#: application/data/ui/EmptyView.ui:13
msgid "Clean Your Traces"
msgstr ""

#: application/data/ui/EmptyView.ui:39
msgid "Learn more about metadata and the cleaning process limitations"
msgstr ""

#: application/data/ui/FileRow.ui:11
msgid "Remove file from list"
msgstr ""

#: application/data/ui/FileRow.ui:128
msgid "Warning"
msgstr ""

#: application/data/ui/FileRow.ui:146
msgid "Error"
msgstr ""

#: application/data/ui/FileRow.ui:186
msgid "Cleaned"
msgstr ""

#: application/data/ui/MenuButton.ui:10
msgid "_New Window"
msgstr ""

#: application/data/ui/MenuButton.ui:14
msgid "_Clear Window"
msgstr ""

#: application/data/ui/MenuButton.ui:20
msgid "_Help"
msgstr ""

#: application/data/ui/MenuButton.ui:25
msgid "_Keyboard Shortcuts"
msgstr ""

#: application/data/ui/MenuButton.ui:29
msgid "_About Metadata Cleaner"
msgstr ""

#: application/data/ui/MenuButton.ui:38
msgid "Main Menu"
msgstr ""

#: application/data/ui/SettingsButton.ui:9
msgid "Cleaning settings"
msgstr ""

#: application/data/ui/SettingsButton.ui:27
msgid "Lightweight Cleaning"
msgstr ""

#: application/data/ui/SettingsButton.ui:32
msgid "Learn more about the lightweight cleaning"
msgstr ""

#: application/data/ui/Window.ui:80
msgid "Details"
msgstr ""

#: application/data/ui/Window.ui:86
msgid "Close"
msgstr ""

#. Translators: Replace `translator-credits` by your name, and optionally your email address between angle brackets (Example: `Name <mail@example.org>`) or your website (Example: `Name https://example.org/`). If names are already present, do not remove them and add yours on a new line.
#: application/data/ui/Window.ui:125
msgid "translator-credits"
msgstr ""

#: application/data/ui/Window.ui:128
msgid "Choose files to clean"
msgstr ""

#: application/data/ui/Window.ui:138
msgid "Choose folders to clean"
msgstr ""

#: application/metadatacleaner/modules/file.py:244
msgid "An error occured during the cleaning."
msgstr ""

#: application/metadatacleaner/modules/file.py:247
msgid "Something bad happened during the cleaning, cleaned file not found"
msgstr ""

#: application/metadatacleaner/ui/detailsview.py:58
msgid "The File Has Been Cleaned"
msgstr ""

#: application/metadatacleaner/ui/detailsview.py:60
msgid ""
"Known metadata have been removed, however the cleaning process has some "
"limitations."
msgstr ""

#: application/metadatacleaner/ui/detailsview.py:63
msgid "Learn more"
msgstr ""

#: application/metadatacleaner/ui/detailsview.py:76
msgid "Unable to Read the File"
msgstr ""

#: application/metadatacleaner/ui/detailsview.py:77
msgid "File Type not Supported"
msgstr ""

#: application/metadatacleaner/ui/detailsview.py:79
msgid "Unable to Check for Metadata"
msgstr ""

#: application/metadatacleaner/ui/detailsview.py:80
msgid "No Known Metadata"
msgstr ""

#: application/metadatacleaner/ui/detailsview.py:82
msgid "Unable to Remove Metadata"
msgstr ""

#: application/metadatacleaner/ui/detailsview.py:86
msgid "The file will be cleaned anyway to be sure."
msgstr ""

#: application/metadatacleaner/ui/filechooserdialog.py:24
msgid "All supported files"
msgstr ""

#: application/metadatacleaner/ui/folderchooserdialog.py:22
msgid "Add files from subfolders"
msgstr ""

#: application/metadatacleaner/ui/statusindicator.py:37
msgid "Processing file {}/{}"
msgstr ""

#: application/metadatacleaner/ui/statusindicator.py:39
msgid "Cleaning file {}/{}"
msgstr ""

#: application/metadatacleaner/ui/statusindicator.py:79
#, python-format
msgid "%i file cleaned."
msgid_plural "%i files cleaned."
msgstr[0] ""
msgstr[1] ""

#: application/metadatacleaner/ui/statusindicator.py:84
#, python-format
msgid "%i error occured."
msgid_plural "%i errors occured."
msgstr[0] ""
msgstr[1] ""

#: application/metadatacleaner/ui/window.py:112
msgid "Libraries"
msgstr ""
