# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fr.romainvigier.MetadataCleaner package.
# albanobattistella <albano_battistella@hotmail.com>, 2021, 2022, 2023.
# Romain Vigier <romain@romainvigier.fr>, 2021.
# Andrea Andre <andrea.tsg19@slmail.me>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: fr.romainvigier.MetadataCleaner\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-11 18:41+0000\n"
"PO-Revision-Date: 2023-11-17 17:03+0000\n"
"Last-Translator: Andrea Andre <andrea.tsg19@slmail.me>\n"
"Language-Team: Italian <https://hosted.weblate.org/projects/metadata-cleaner/"
"application/it/>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.2\n"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:6
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:7
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:9
#: application/data/ui/Window.ui:107 application/metadatacleaner/app.py:38
msgid "Metadata Cleaner"
msgstr "Pulitore di Metadati"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:8
msgid "Clean metadata from your files"
msgstr "Pulisci i metadati dai tuoi file"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:15
msgid "Metadata;Remover;Cleaner;"
msgstr "Metadata;Remover;Cleaner;Metadati;Rimuovitore;Pulitore;"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:10
msgid "View and clean metadata in files"
msgstr "Visualizza e pulisci i metadati nei file"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:26
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"I metadati all'interno di un file possono dire molto su di te. Le fotocamere "
"registrano dati su quando e dove è stata scattata una foto e quale "
"fotocamera è stata utilizzata. Le applicazioni di Office aggiungono "
"automaticamente informazioni sull'autore e sull'azienda a documenti e fogli "
"di calcolo. Si tratta di informazioni sensibili che non vorresti divulgare."

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:27
msgid ""
"This tool allows you to view metadata in your files and to get rid of it, as "
"much as possible."
msgstr ""
"Questo strumento ti consente di visualizzare i metadati nei tuoi file e di "
"liberartene, per quanto possibile."

#: application/data/gtk/help-overlay.ui:15
msgid "Files"
msgstr "File"

#: application/data/gtk/help-overlay.ui:18
#: application/data/ui/AddFilesButton.ui:49
msgid "Add files"
msgstr "Aggiungi file"

#: application/data/gtk/help-overlay.ui:24
#: application/data/ui/AddFilesButton.ui:56
msgid "Add folders"
msgstr "Aggiungi cartelle"

#: application/data/gtk/help-overlay.ui:30
msgid "Clean metadata"
msgstr "Pulisci i metadati"

#: application/data/gtk/help-overlay.ui:36
msgid "Clear all files from window"
msgstr "Cancella tutti i file dalla finestra"

#: application/data/gtk/help-overlay.ui:44
msgid "General"
msgstr "Generali"

#: application/data/gtk/help-overlay.ui:47
msgid "New window"
msgstr "Nuova finestra"

#: application/data/gtk/help-overlay.ui:53
msgid "Close window"
msgstr "Chiudi finestra"

#: application/data/gtk/help-overlay.ui:59
msgid "Quit"
msgstr "Esci"

#: application/data/gtk/help-overlay.ui:65
msgid "Keyboard shortcuts"
msgstr "Scorciatoie da tastiera"

#: application/data/gtk/help-overlay.ui:71
msgid "Help"
msgstr "Aiuto"

#: application/data/ui/AddFilesButton.ui:21
msgid "_Add Files"
msgstr "_Aggiungi file"

#: application/data/ui/AddFilesButton.ui:33
msgid "Add _Folders"
msgstr "Aggiungi _cartelle"

#: application/data/ui/CleanMetadataButton.ui:9
msgid "_Clean"
msgstr "_Pulisci"

#: application/data/ui/CleaningWarningDialog.ui:9
msgid "Make sure you backed up your files!"
msgstr "Assicurati di aver eseguito il backup dei tuoi file!"

#: application/data/ui/CleaningWarningDialog.ui:10
msgid "Once the files are cleaned, there's no going back."
msgstr ""
"Una volta che i file sono stati puliti, non è possibile tornare indietro."

#: application/data/ui/CleaningWarningDialog.ui:17
msgid "Don't tell me again"
msgstr "Non dirmelo di nuovo"

#: application/data/ui/CleaningWarningDialog.ui:25
#: application/data/ui/StatusIndicator.ui:37
msgid "Cancel"
msgstr "Cancella"

#: application/data/ui/CleaningWarningDialog.ui:30
msgid "Clean"
msgstr "Pulisci"

#: application/data/ui/EmptyView.ui:13
msgid "Clean Your Traces"
msgstr "Pulisci le tue tracce"

#: application/data/ui/EmptyView.ui:39
msgid "Learn more about metadata and the cleaning process limitations"
msgstr ""
"Ulteriori informazioni sui metadati e sulle limitazioni del processo di "
"pulizia"

#: application/data/ui/FileRow.ui:11
msgid "Remove file from list"
msgstr "Rimuovi file dall'elenco"

#: application/data/ui/FileRow.ui:128
msgid "Warning"
msgstr "Avvertimento"

#: application/data/ui/FileRow.ui:146
msgid "Error"
msgstr "Errore"

#: application/data/ui/FileRow.ui:186
msgid "Cleaned"
msgstr "Pulito"

#: application/data/ui/MenuButton.ui:10
msgid "_New Window"
msgstr "_Nuova finestra"

#: application/data/ui/MenuButton.ui:14
msgid "_Clear Window"
msgstr "_Pulisci finestra"

#: application/data/ui/MenuButton.ui:20
msgid "_Help"
msgstr "_Aiuto"

#: application/data/ui/MenuButton.ui:25
msgid "_Keyboard Shortcuts"
msgstr "_Scorciatoie da tastiera"

#: application/data/ui/MenuButton.ui:29
msgid "_About Metadata Cleaner"
msgstr "_Informazioni su Pulitore di Metadati"

#: application/data/ui/MenuButton.ui:38
msgid "Main Menu"
msgstr "Opzioni"

#: application/data/ui/SettingsButton.ui:9
msgid "Cleaning settings"
msgstr "Impostazioni di pulizia"

#: application/data/ui/SettingsButton.ui:27
msgid "Lightweight Cleaning"
msgstr "Pulizia leggera"

#: application/data/ui/SettingsButton.ui:32
msgid "Learn more about the lightweight cleaning"
msgstr "Scopri di più sulla pulizia leggera"

#: application/data/ui/Window.ui:80
msgid "Details"
msgstr "Dettagli"

#: application/data/ui/Window.ui:86
msgid "Close"
msgstr "Chiudi"

#. Translators: Replace `translator-credits` by your name, and optionally your email address between angle brackets (Example: `Name <mail@example.org>`) or your website (Example: `Name https://example.org/`). If names are already present, do not remove them and add yours on a new line.
#: application/data/ui/Window.ui:125
msgid "translator-credits"
msgstr "Albano Battistella <albano_battistella@hotmail.com>"

#: application/data/ui/Window.ui:128
msgid "Choose files to clean"
msgstr "Scegli i file da pulire"

#: application/data/ui/Window.ui:138
msgid "Choose folders to clean"
msgstr "Scegli le cartelle da pulire"

#: application/metadatacleaner/modules/file.py:244
msgid "An error occured during the cleaning."
msgstr "Si è verificato un errore durante la pulizia."

#: application/metadatacleaner/modules/file.py:247
msgid "Something bad happened during the cleaning, cleaned file not found"
msgstr ""
"È successo qualcosa di brutto durante la pulizia, file pulito non trovato"

#: application/metadatacleaner/ui/detailsview.py:58
msgid "The File Has Been Cleaned"
msgstr "Il file è stato pulito"

#: application/metadatacleaner/ui/detailsview.py:60
msgid ""
"Known metadata have been removed, however the cleaning process has some "
"limitations."
msgstr ""
"I metadati noti sono stati rimossi, tuttavia il processo di pulizia presenta "
"alcune limitazioni."

#: application/metadatacleaner/ui/detailsview.py:63
msgid "Learn more"
msgstr "Ulteriori informazioni"

#: application/metadatacleaner/ui/detailsview.py:76
msgid "Unable to Read the File"
msgstr "Impossibile leggere il file"

#: application/metadatacleaner/ui/detailsview.py:77
msgid "File Type not Supported"
msgstr "Tipo di file non accettato"

#: application/metadatacleaner/ui/detailsview.py:79
msgid "Unable to Check for Metadata"
msgstr "Impossibile controllare i metadati"

#: application/metadatacleaner/ui/detailsview.py:80
msgid "No Known Metadata"
msgstr "Metadati sconosciuti"

#: application/metadatacleaner/ui/detailsview.py:82
msgid "Unable to Remove Metadata"
msgstr "Impossibile rimuovere i metadati"

#: application/metadatacleaner/ui/detailsview.py:86
msgid "The file will be cleaned anyway to be sure."
msgstr "Il file verrà comunque pulito per sicurezza."

#: application/metadatacleaner/ui/filechooserdialog.py:24
msgid "All supported files"
msgstr "Tutti i file accettati"

#: application/metadatacleaner/ui/folderchooserdialog.py:22
msgid "Add files from subfolders"
msgstr "Aggiungi file da sottocartelle"

#: application/metadatacleaner/ui/statusindicator.py:37
msgid "Processing file {}/{}"
msgstr "Elaborazione file {}/{}"

#: application/metadatacleaner/ui/statusindicator.py:39
msgid "Cleaning file {}/{}"
msgstr "Pulizia file {}/{}"

#: application/metadatacleaner/ui/statusindicator.py:79
#, python-format
msgid "%i file cleaned."
msgid_plural "%i files cleaned."
msgstr[0] "%i file pulito."
msgstr[1] "%i file puliti."

#: application/metadatacleaner/ui/statusindicator.py:84
#, python-format
msgid "%i error occured."
msgid_plural "%i errors occured."
msgstr[0] "Si è verificato %i errore."
msgstr[1] "Si sono verificati %i errori."

#: application/metadatacleaner/ui/window.py:112
msgid "Libraries"
msgstr "Librerie"

#~ msgid "Clean without warning"
#~ msgstr "Pulisci senza preavviso"

#~ msgid "Clean the files without showing the warning dialog"
#~ msgstr "Pulisci i file senza mostrare la finestra di avviso"

#~ msgid "Lightweight cleaning"
#~ msgstr "Modalità leggera"

#~ msgid "Don't make destructive changes to files but may leave some metadata"
#~ msgstr ""
#~ "Non apportare modifiche distruttive ai file ma potrebbe lasciare alcuni "
#~ "metadati"

#~ msgid "Window width"
#~ msgstr "Larghezza della finestra"

#~ msgid "Saved width of the window"
#~ msgstr "Larghezza salvata della finestra"

#~ msgid "Window height"
#~ msgstr "Altezza della finestra"

#~ msgid "Saved height of the window"
#~ msgstr "Altezza della finestra salvata"

#~ msgid "Updated translations"
#~ msgstr "Traduzioni aggiornate"

#~ msgid "Improved user interface"
#~ msgstr "Interfaccia utente migliorata"

#~ msgid "New translations"
#~ msgstr "Nuove traduzioni"

#~ msgid "Bug fixes"
#~ msgstr "Correzioni di bug"

#~ msgid "New button to add folders"
#~ msgstr "Nuovo pulsante per aggiungere cartelle"

#~ msgid "Improved adaptive user interface"
#~ msgstr "Interfaccia utente adattiva migliorata"

#~ msgid "New help pages"
#~ msgstr "Nuove pagine di aiuto"

#~ msgid "One-click cleaning, no need to save after cleaning"
#~ msgstr "Pulizia con un clic, non è necessario salvare dopo la pulizia"

#~ msgid "Persistent lightweight cleaning option"
#~ msgstr "Opzione di pulizia leggera persistente"

#~ msgid "Files with uppercase extension can now be added"
#~ msgstr "È ora possibile aggiungere file con estensione maiuscola"

#~ msgid "About"
#~ msgstr "Informazioni"

#~ msgid "Chat on Matrix"
#~ msgstr "Chatta su Matrix"

#~ msgid "View the code on GitLab"
#~ msgstr "Visualizza il codice su GitLab"

#~ msgid "Translate on Weblate"
#~ msgstr "Traduci su Weblate"

#~ msgid "Support us on Liberapay"
#~ msgstr "Sostienici su Liberapay"

#~ msgid "Credits"
#~ msgstr "Crediti"

#~ msgid "Code"
#~ msgstr "Codice"

#~ msgid "Artwork"
#~ msgstr "Artwork"

#~ msgid "Documentation"
#~ msgstr "Documentazione"

#~ msgid "Translation"
#~ msgstr "Traduzione"

#~ msgid ""
#~ "This program uses <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> to "
#~ "parse and clean the metadata. Show them some love!"
#~ msgstr ""
#~ "Questo programma utilizza <a href=\"https://0xacab.org/jvoisin/"
#~ "mat2\">mat2</a> per pulire i metadati. Mostra loro un po' d'amore!"

#~ msgid ""
#~ "The source code of this program is released under the terms of the <a "
#~ "href=\"https://www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 or later</"
#~ "a>. The original artwork and translations are released under the terms of "
#~ "the <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA "
#~ "4.0</a>."
#~ msgstr ""
#~ "Il codice sorgente di questo programma è rilasciato sotto i termini della "
#~ "<a href=\"https://www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 o più "
#~ "aggiornata</a>. L'opera d'arte originale e le traduzioni sono rilasciate "
#~ "secondo i termini del <a href=\"https://creativecommons.org/licenses/by-"
#~ "sa/4.0/\">CC BY-SA 4.0</a>."

#~ msgid "Adding files…"
#~ msgstr "Aggiungere file…"

#~ msgid "Croatian translation (contributed by Milo Ivir)"
#~ msgstr "Traduzione croata (contributo di Milo Ivir)"

#~ msgid "Portuguese (Brazil) translation (contributed by Gustavo Costa)"
#~ msgstr "Traduzione portoghese (Brasile) (contributo di Gustavo Costa)"

#~ msgid "New in v1.0.4:"
#~ msgstr "Novità nella v1.0.4:"

#~ msgid "Turkish translation (contributed by Oğuz Ersen)"
#~ msgstr "Traduzione in turco (contributo di Oğuz Ersen)"

#~ msgid "New in v1.0.2:"
#~ msgstr "Novità nella v1.0.2:"

#~ msgid "Spanish translation (contributed by Óscar Fernández Díaz)"
#~ msgstr "Traduzione spagnola (contributo di Óscar Fernández Díaz)"

#~ msgid "Swedish translation (contributed by Åke Engelbrektson)"
#~ msgstr "Traduzione svedese (contributo di Åke Engelbrektson)"

#~ msgid "New in v1.0.1:"
#~ msgstr "Novità nella v1.0.1:"

#~ msgid "German translation (contributed by lux)"
#~ msgstr "Traduzione in tedesco (contributo di lux)"

#~ msgid ""
#~ "Trade some metadata's presence in exchange of the guarantee that the data "
#~ "won't be modified"
#~ msgstr ""
#~ "Scambia la presenza di alcuni metadati in cambio della garanzia che i "
#~ "dati non verranno modificati"

#~ msgid "Warn before saving cleaned files"
#~ msgstr "Avvisa prima di salvare i file puliti"

#~ msgid "Show the warning dialog before saving the cleaned files"
#~ msgstr "Mostra la finestra di avviso prima di salvare i file puliti"

#~ msgid ""
#~ "The GitLab, Matrix, Liberapay and Weblate logos and names are trademarks "
#~ "of their respective owners."
#~ msgstr ""
#~ "I loghi e i nomi GitLab, Matrix, Liberapay e Weblate sono marchi dei "
#~ "rispettivi proprietari."

#~ msgid "Romain Vigier"
#~ msgstr "Romain Vigier"

#~ msgid "Note about metadata and privacy"
#~ msgstr "Nota sui metadati e sulla privacy"

#~ msgid ""
#~ "Metadata consist of information that characterizes data. Metadata are "
#~ "used to provide documentation for data products. In essence, metadata "
#~ "answer who, what, when, where, why, and how about every facet of the data "
#~ "that are being documented.\n"
#~ "\n"
#~ "Metadata within a file can tell a lot about you. Cameras record data "
#~ "about when a picture was taken and what camera was used. Office "
#~ "aplications automatically add author and company information to documents "
#~ "and spreadsheets. Maybe you don't want to disclose those informations.\n"
#~ "\n"
#~ "This tool will get rid, as much as possible, of metadata."
#~ msgstr ""
#~ "I metadati sono informazioni che caratterizzano i dati. I metadati "
#~ "vengono utilizzati per fornire documentazione per i prodotti di dati. In "
#~ "sostanza, i metadati rispondono a chi, cosa, quando, dove, perché e come "
#~ "su ogni aspetto dei dati che vengono documentati.\n"
#~ "\n"
#~ "I metadati all'interno di un file possono dire molto su di te. Le "
#~ "fotocamere registrano i dati su quando è stata scattata una foto e quale "
#~ "fotocamera è stata utilizzata. Le applicazioni di Office aggiungono "
#~ "automaticamente informazioni sull'autore e sull'azienda a documenti e "
#~ "fogli di calcolo. Forse non vuoi divulgare queste informazioni.\n"
#~ "\n"
#~ "Questo strumento eliminerà, per quanto possibile, i metadati."

#~ msgid ""
#~ "While this tool is doing its very best to display metadata, it doesn't "
#~ "mean that a file is clean from any metadata if it doesn't show any. There "
#~ "is no reliable way to detect every single possible metadata for complex "
#~ "file formats.\n"
#~ "\n"
#~ "This is why you shouldn't rely on metadata's presence to decide if your "
#~ "file must be cleaned or not."
#~ msgstr ""
#~ "Sebbene questo strumento stia facendo del suo meglio per visualizzare i "
#~ "metadati, ciò non significa che un file sia pulito da qualsiasi metadato "
#~ "se non ne mostra alcuno. Non esiste un modo affidabile per rilevare ogni "
#~ "singolo metadato possibile per formati di file complessi.\n"
#~ "\n"
#~ "Questo è il motivo per cui non dovresti fare affidamento sulla presenza "
#~ "dei metadati per decidere se il tuo file deve essere pulito o meno."

#~ msgid ""
#~ "By default, the removal process might alter a bit the data of your files, "
#~ "in order to remove as much metadata as possible. For example, texts in "
#~ "PDF might not be selectable anymore, compressed images might get "
#~ "compressed again… If you're willing to trade some metadata's presence in "
#~ "exchange of the guarantee that the data of your files won't be modified, "
#~ "the lightweight mode precisely does that."
#~ msgstr ""
#~ "Per impostazione predefinita, il processo di rimozione potrebbe alterare "
#~ "leggermente i dati dei tuoi file, al fine di rimuovere quanti più "
#~ "metadati possibile. Ad esempio, i testi in PDF potrebbero non essere più "
#~ "selezionabili, le immagini compresse potrebbero comprimersi di nuovo... "
#~ "Se sei disposto a scambiare la presenza di alcuni metadati in cambio "
#~ "della garanzia che i dati dei tuoi file non verranno modificati, la "
#~ "modalità leggera appunto lo fa."

#~ msgid "Note about _metadata and privacy"
#~ msgstr "Nota su _metadati e privacy"

#~ msgid "Note about _removing metadata"
#~ msgstr "Nota sulla _rimozione dei metadati"

#~ msgid "Metadata details"
#~ msgstr "Dettagli metadati"

#~ msgid "_Save"
#~ msgstr "_Salva"

#~ msgctxt "shortcut window"
#~ msgid "Add files"
#~ msgstr "Aggiungi file"

#~ msgctxt "shortcut window"
#~ msgid "Save cleaned files"
#~ msgstr "Salva i file puliti"

#~ msgid "Done!"
#~ msgstr "Fatto!"

#~ msgid "Initializing…"
#~ msgstr "Inizializzazione in corso…"

#~ msgid "Error while initializing the file parser."
#~ msgstr "Errore durante l'inizializzazione dell'analisi del file."

#~ msgid "File type supported."
#~ msgstr "Tipo di file supportato."

#~ msgid "Checking metadata…"
#~ msgstr "Controllo dei metadati…"

#~ msgid "Removing metadata…"
#~ msgstr "Rimozione dei metadati…"

#~ msgid "Error while removing metadata:"
#~ msgstr "Errore durante la rimozione dei metadati:"

#~ msgid "Saving the cleaned file…"
#~ msgstr "Salvataggio del file pulito…"

#~ msgid "Error while saving the file:"
#~ msgstr "Errore durante il salvataggio del file:"

#~ msgid "The cleaned file has been saved."
#~ msgstr "Il file pulito è stato salvato."

#~ msgid "{filename}:"
#~ msgstr "{filename}:"
