# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fr.romainvigier.MetadataCleaner package.
# Allan Nordhøy <epost@anotheragency.no>, 2021, 2022.
# Romain Vigier <romain@romainvigier.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: fr.romainvigier.MetadataCleaner\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-11 18:41+0000\n"
"PO-Revision-Date: 2022-04-05 10:09+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/"
"metadata-cleaner/application/nb_NO/>\n"
"Language: nb_NO\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.12-dev\n"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:6
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:7
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:9
#: application/data/ui/Window.ui:107 application/metadatacleaner/app.py:38
msgid "Metadata Cleaner"
msgstr "Metadatarenser"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:8
msgid "Clean metadata from your files"
msgstr "Fjern metadata fra filene dine"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:15
msgid "Metadata;Remover;Cleaner;"
msgstr "Metadata;renser;fjerning;"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:10
msgid "View and clean metadata in files"
msgstr "Vis og rens metadata i filer"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:26
#, fuzzy
#| msgid ""
#| "Metadata within a file can tell a lot about you. Cameras record data "
#| "about when a picture was taken and what camera was used. Office "
#| "applications automatically add author and company information to "
#| "documents and spreadsheets. Maybe you don't want to disclose those "
#| "informations."
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"Metadata i en fil kan fortelle mye om deg. Kameraer registrerer data om når "
"et bilde ble tatt, og hvilket kamera som ble brukt. Kontorprogrammer legger "
"automatisk til utvikler- og bedrifts-info i dokumenter og regneark. Kanskje "
"du ikke ønsker å viderebringe den infoen?"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:27
#, fuzzy
msgid ""
"This tool allows you to view metadata in your files and to get rid of it, as "
"much as possible."
msgstr ""
"Dette verktøyet lar deg vise metadata i filene dine og å slette dem, så mye "
"som mulig."

#: application/data/gtk/help-overlay.ui:15
#, fuzzy
msgid "Files"
msgstr "Filer"

#: application/data/gtk/help-overlay.ui:18
#: application/data/ui/AddFilesButton.ui:49
msgid "Add files"
msgstr "Legg til filer"

#: application/data/gtk/help-overlay.ui:24
#: application/data/ui/AddFilesButton.ui:56
#, fuzzy
#| msgid "Add files"
msgid "Add folders"
msgstr "Legg til filer"

#: application/data/gtk/help-overlay.ui:30
#, fuzzy
msgid "Clean metadata"
msgstr "Rens metadata"

#: application/data/gtk/help-overlay.ui:36
#, fuzzy
msgid "Clear all files from window"
msgstr "Rens alle filer fra vinduet"

#: application/data/gtk/help-overlay.ui:44
#, fuzzy
msgid "General"
msgstr "Generelt"

#: application/data/gtk/help-overlay.ui:47
#, fuzzy
msgid "New window"
msgstr "Nytt vindu"

#: application/data/gtk/help-overlay.ui:53
#, fuzzy
msgid "Close window"
msgstr "Lukk vindu"

#: application/data/gtk/help-overlay.ui:59
#, fuzzy
msgid "Quit"
msgstr "Avslutt"

#: application/data/gtk/help-overlay.ui:65
#, fuzzy
msgid "Keyboard shortcuts"
msgstr "Tastatursnarveier"

#: application/data/gtk/help-overlay.ui:71
msgid "Help"
msgstr "Hjelp"

#: application/data/ui/AddFilesButton.ui:21
#, fuzzy
#| msgid "Add files"
msgid "_Add Files"
msgstr "Legg til filer"

#: application/data/ui/AddFilesButton.ui:33
#, fuzzy
#| msgid "Add files"
msgid "Add _Folders"
msgstr "Legg til filer"

#: application/data/ui/CleanMetadataButton.ui:9
msgid "_Clean"
msgstr "_Rens"

#: application/data/ui/CleaningWarningDialog.ui:9
msgid "Make sure you backed up your files!"
msgstr "Forsikre deg om at du har sikkerhetskopiert filene dine."

#: application/data/ui/CleaningWarningDialog.ui:10
msgid "Once the files are cleaned, there's no going back."
msgstr "Når filene er renset er det ingen vei tilbake."

#: application/data/ui/CleaningWarningDialog.ui:17
msgid "Don't tell me again"
msgstr "Ikke vis igjen"

#: application/data/ui/CleaningWarningDialog.ui:25
#: application/data/ui/StatusIndicator.ui:37
msgid "Cancel"
msgstr "Avbryt"

#: application/data/ui/CleaningWarningDialog.ui:30
#, fuzzy
msgid "Clean"
msgstr "_Rens"

#: application/data/ui/EmptyView.ui:13
#, fuzzy
#| msgid "Clean your traces."
msgid "Clean Your Traces"
msgstr "Rens opp etter deg."

#: application/data/ui/EmptyView.ui:39
#, fuzzy
#| msgid "Learn more about metadata and the cleaning process limitations."
msgid "Learn more about metadata and the cleaning process limitations"
msgstr "Lær mer om metadata og begrensninger i renseprosessen."

#: application/data/ui/FileRow.ui:11
msgid "Remove file from list"
msgstr "Fjern filen fra listen"

#: application/data/ui/FileRow.ui:128
msgid "Warning"
msgstr "Advarsel"

#: application/data/ui/FileRow.ui:146
msgid "Error"
msgstr "Feil"

#: application/data/ui/FileRow.ui:186
msgid "Cleaned"
msgstr "Renset"

#: application/data/ui/MenuButton.ui:10
#, fuzzy
#| msgid "_New window"
msgid "_New Window"
msgstr "_Nytt vindu"

#: application/data/ui/MenuButton.ui:14
#, fuzzy
msgid "_Clear Window"
msgstr "Lukk vindu"

#: application/data/ui/MenuButton.ui:20
msgid "_Help"
msgstr "_Hjelp"

#: application/data/ui/MenuButton.ui:25
#, fuzzy
#| msgid "_Keyboard shortcuts"
msgid "_Keyboard Shortcuts"
msgstr "_Tastatursnarveier"

#: application/data/ui/MenuButton.ui:29
msgid "_About Metadata Cleaner"
msgstr "_Om Metadatarenser"

#: application/data/ui/MenuButton.ui:38
#, fuzzy
#| msgid "Main menu"
msgid "Main Menu"
msgstr "Hovedmeny"

#: application/data/ui/SettingsButton.ui:9
msgid "Cleaning settings"
msgstr "Rensningsinnstillinger"

#: application/data/ui/SettingsButton.ui:27
#, fuzzy
#| msgid "Lightweight cleaning"
msgid "Lightweight Cleaning"
msgstr "Lett rensing"

#: application/data/ui/SettingsButton.ui:32
msgid "Learn more about the lightweight cleaning"
msgstr "Lær mer om den lette renseprosessen"

#: application/data/ui/Window.ui:80
msgid "Details"
msgstr "Detaljer"

#: application/data/ui/Window.ui:86
msgid "Close"
msgstr "Lukk"

#. Translators: Replace `translator-credits` by your name, and optionally your email address between angle brackets (Example: `Name <mail@example.org>`) or your website (Example: `Name https://example.org/`). If names are already present, do not remove them and add yours on a new line.
#: application/data/ui/Window.ui:125
msgid "translator-credits"
msgstr "Allan Nordhøy <epost@anotheragency.no>"

#: application/data/ui/Window.ui:128
#, fuzzy
msgid "Choose files to clean"
msgstr "Velg filer"

#: application/data/ui/Window.ui:138
#, fuzzy
msgid "Choose folders to clean"
msgstr "Velg filer"

#: application/metadatacleaner/modules/file.py:244
msgid "An error occured during the cleaning."
msgstr ""

#: application/metadatacleaner/modules/file.py:247
msgid "Something bad happened during the cleaning, cleaned file not found"
msgstr "Noe gikk galt under renseprosessen. Finner ikke den rensede filen."

#: application/metadatacleaner/ui/detailsview.py:58
#, fuzzy
#| msgid "The file has been cleaned."
msgid "The File Has Been Cleaned"
msgstr "Filen har blitt renset."

#: application/metadatacleaner/ui/detailsview.py:60
#, fuzzy
#| msgid "Learn more about metadata and the cleaning process limitations."
msgid ""
"Known metadata have been removed, however the cleaning process has some "
"limitations."
msgstr "Lær mer om metadata og begrensninger i renseprosessen."

#: application/metadatacleaner/ui/detailsview.py:63
msgid "Learn more"
msgstr "Lær mer"

#: application/metadatacleaner/ui/detailsview.py:76
#, fuzzy
#| msgid "Unable to read the file."
msgid "Unable to Read the File"
msgstr "Kunne ikke lese filen."

#: application/metadatacleaner/ui/detailsview.py:77
#, fuzzy
msgid "File Type not Supported"
msgstr "Filtypen støttes ikke."

#: application/metadatacleaner/ui/detailsview.py:79
#, fuzzy
#| msgid "Unable to check metadata."
msgid "Unable to Check for Metadata"
msgstr "Kunne ikke sjekke metadataen."

#: application/metadatacleaner/ui/detailsview.py:80
#, fuzzy
#| msgid "Metadata"
msgid "No Known Metadata"
msgstr "Metadata"

#: application/metadatacleaner/ui/detailsview.py:82
#, fuzzy
#| msgid "Unable to remove metadata."
msgid "Unable to Remove Metadata"
msgstr "Kunne ikke fjerne metadata."

#: application/metadatacleaner/ui/detailsview.py:86
#, fuzzy
#| msgid "No known metadata, the file will be cleaned to be sure."
msgid "The file will be cleaned anyway to be sure."
msgstr "Ingen kjent metadata. Filen vil bli renset for å være sikker."

#: application/metadatacleaner/ui/filechooserdialog.py:24
msgid "All supported files"
msgstr "Alle støttede filer"

#: application/metadatacleaner/ui/folderchooserdialog.py:22
msgid "Add files from subfolders"
msgstr "Legg til filer fra undermapper"

#: application/metadatacleaner/ui/statusindicator.py:37
msgid "Processing file {}/{}"
msgstr "Behandler fil {}/{}"

#: application/metadatacleaner/ui/statusindicator.py:39
#, fuzzy
#| msgid "Processing file {}/{}"
msgid "Cleaning file {}/{}"
msgstr "Behandler fil {}/{}"

#: application/metadatacleaner/ui/statusindicator.py:79
#, python-format
msgid "%i file cleaned."
msgid_plural "%i files cleaned."
msgstr[0] ""
msgstr[1] ""

#: application/metadatacleaner/ui/statusindicator.py:84
#, python-format
msgid "%i error occured."
msgid_plural "%i errors occured."
msgstr[0] ""
msgstr[1] ""

#: application/metadatacleaner/ui/window.py:112
msgid "Libraries"
msgstr ""

#~ msgid "Clean without warning"
#~ msgstr "Rens uten advarsel"

#~ msgid "Clean the files without showing the warning dialog"
#~ msgstr "Rens filene uten å vise advarselsdialogen"

#~ msgid "Lightweight cleaning"
#~ msgstr "Lett rensing"

#, fuzzy
#~ msgid "Don't make destructive changes to files but may leave some metadata"
#~ msgstr ""
#~ "Ikke utfør noen destruktive endringer i filer, men risiker å levne litt "
#~ "metadata."

#~ msgid "Window width"
#~ msgstr "Vindusbredde"

#~ msgid "Saved width of the window"
#~ msgstr "Lagret vindusbredde"

#~ msgid "Window height"
#~ msgstr "Vindushøyde"

#~ msgid "Saved height of the window"
#~ msgstr "Lagret vindushøyde"

#, fuzzy
#~| msgid "New translations"
#~ msgid "Updated translations"
#~ msgstr "Nye oversettelser"

#, fuzzy
#~| msgid "Improved adaptive user interface"
#~ msgid "Improved user interface"
#~ msgstr "Forbedret tillpassbart brukergrensesnitt"

#~ msgid "New translations"
#~ msgstr "Nye oversettelser"

#, fuzzy
#~ msgid "Bug fixes"
#~ msgstr "Feilfikser:"

#~ msgid "New button to add folders"
#~ msgstr "Ny knapp for å legge til mapper"

#~ msgid "Improved adaptive user interface"
#~ msgstr "Forbedret tillpassbart brukergrensesnitt"

#~ msgid "New help pages"
#~ msgstr "Nye hjelpesider"

#, fuzzy
#~| msgid "One-click cleaning, no need to clean then save anymore"
#~ msgid "One-click cleaning, no need to save after cleaning"
#~ msgstr "Rensing med ett klikk. Ingen grunn til å rense for så å lagre."

#, fuzzy
#~| msgid "Lightweight cleaning option is now saved"
#~ msgid "Persistent lightweight cleaning option"
#~ msgstr "Valg om lett rensing er nå vedvarende"

#~ msgid "Files with uppercase extension can now be added"
#~ msgstr "Filer med filendelser som bruker store bokstaver kan nå legges til"

#~ msgid "Chat on Matrix"
#~ msgstr "Sludre på Matrix"

#~ msgid "View the code on GitLab"
#~ msgstr "Vis koden på GitLab"

#~ msgid "Translate on Weblate"
#~ msgstr "Oversett på Weblate"

#~ msgid "Support us on Liberapay"
#~ msgstr "Støtt oss på Liberapay"

#~ msgid "Code"
#~ msgstr "Kode"

#~ msgid "Artwork"
#~ msgstr "Kunstverk"

#~ msgid "Documentation"
#~ msgstr "Dokumentasjon"

#~ msgid "Translation"
#~ msgstr "Oversettelse"

#~ msgid ""
#~ "This program uses <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> to "
#~ "parse and clean the metadata. Show them some love!"
#~ msgstr ""
#~ "Programmet bruker <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> "
#~ "for å tolke og tømme metadataen. Ha miskunn."

#~ msgid ""
#~ "The source code of this program is released under the terms of the <a "
#~ "href=\"https://www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 or later</"
#~ "a>. The original artwork and translations are released under the terms of "
#~ "the <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA "
#~ "4.0</a>."
#~ msgstr ""
#~ "Lisensiert <a href=\"https://www.gnu.org/licenses/gpl-3.0.html\">GPLv3+</"
#~ "a>. Kunsten og oversettelsen er utgitt som<a href=\"https://"
#~ "creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA 4.0</a>."

#, fuzzy
#~| msgid "Add files"
#~ msgid "Adding files…"
#~ msgstr "Legg til filer"

#~ msgid "Croatian translation (contributed by Milo Ivir)"
#~ msgstr "Kroatisk oversettelse ved Milo Ivir"

#, fuzzy
#~ msgid "Portuguese (Brazil) translation (contributed by Gustavo Costa)"
#~ msgstr "Brasiliansk-portugisisk oversettelse ved Gustavo Costa"

#~ msgid "New in v1.0.4:"
#~ msgstr "Nytt i v1.0.4:"

#~ msgid "Turkish translation (contributed by Oğuz Ersen)"
#~ msgstr "Tyrkisk oversettelse ved Oğuz Ersen"

#~ msgid "New in v1.0.2:"
#~ msgstr "Nytt i v1.0.2:"

#~ msgid "Spanish translation (contributed by Óscar Fernández Díaz)"
#~ msgstr "Spansk oversettelse ved Óscar Fernández Díaz"

#~ msgid "Swedish translation (contributed by Åke Engelbrektson)"
#~ msgstr "Svensk oversettelse ved Åke Engelbrektson"

#~ msgid "New in v1.0.1:"
#~ msgstr "Nytt i v1.0.1:"

#~ msgid "German translation (contributed by lux)"
#~ msgstr "Tysk oversettelse ved lux"

#, fuzzy
#~ msgid ""
#~ "Trade some metadata's presence in exchange of the guarantee that the data "
#~ "won't be modified"
#~ msgstr ""
#~ "La noe metadata bli igjen, når det betyr at filens data aldri endres."

#~ msgid "Warn before saving cleaned files"
#~ msgstr "Advar før lagring av rensede filer"

#~ msgid "Show the warning dialog before saving the cleaned files"
#~ msgstr "Vis advarselsdialog før lagring av rensede filer"

#~ msgid ""
#~ "The GitLab, Matrix, Liberapay and Weblate logos and names are trademarks "
#~ "of their respective owners."
#~ msgstr ""
#~ "GitLab, Matrix, Liberapay og Weblate-logoene og navnene er varemerker "
#~ "tilhørende sine respektive eiere."

#~ msgid "Romain Vigier"
#~ msgstr "Romain Vigier"

#~ msgid "Metadata details"
#~ msgstr "Metadata-detaljer"

#~ msgid "_Save"
#~ msgstr "_Lagre"

#~ msgctxt "shortcut window"
#~ msgid "Add files"
#~ msgstr "Legg til filer"

#~ msgctxt "shortcut window"
#~ msgid "Save cleaned files"
#~ msgstr "Lagre rensede filer"

#, fuzzy
#~ msgid "File type supported."
#~ msgstr "Støttet filtype."

#~ msgid "Checking metadata…"
#~ msgstr "Renser metadata …"

#~ msgid "Removing metadata…"
#~ msgstr "Fjerner metadata …"

#~ msgid "Saving the cleaned file…"
#~ msgstr "Lagrer den rensede filen …"

#~ msgid "Error while saving the file:"
#~ msgstr "Klarte ikke å lagre filen:"

#~ msgid "The cleaned file has been saved."
#~ msgstr "Den rensede filen har blitt lagret."

#~ msgid "{filename}:"
#~ msgstr "{filename}:"
