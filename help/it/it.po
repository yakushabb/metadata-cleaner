# albanobattistella <albano_battistella@hotmail.com>, 2021, 2023.
# Romain Vigier <romain@romainvigier.fr>, 2021.
# Victor Mihalache <vmhl.ph@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-09-11 18:41+0000\n"
"PO-Revision-Date: 2023-02-19 13:16+0000\n"
"Last-Translator: albanobattistella <albano_battistella@hotmail.com>\n"
"Language-Team: Italian <https://hosted.weblate.org/projects/metadata-cleaner/"
"help/it/>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.16-dev\n"

#. (itstool) path: section/title
#: ../help/C/general.page:14
msgid "Metadata and privacy"
msgstr "Metadati e privacy"

#. (itstool) path: section/p
#: ../help/C/general.page:15
msgid ""
"Metadata consists of information that characterizes data. Metadata is used "
"to provide documentation for data products. In essence, metadata answers "
"who, what, when, where, why, and how about every facet of the data that is "
"being documented."
msgstr ""
"I metadati sono informazioni che caratterizzano i dati. I metadati vengono "
"utilizzati per fornire documentazione per i prodotti di dati. In sostanza, i "
"metadati rispondono a chi, cosa, quando, dove, perché e come su ogni aspetto "
"dei dati che vengono documentati."

#. (itstool) path: section/p
#: ../help/C/general.page:16
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"I metadati all'interno di un file possono dire molto su di te. Le fotocamere "
"registrano dati su quando e dove è stata scattata una foto e quale "
"fotocamera è stata utilizzata. Le applicazioni di Office aggiungono "
"automaticamente informazioni sull'autore e sull'azienda a documenti e fogli "
"di calcolo. Si tratta di informazioni sensibili che non vorresti divulgare."

#. (itstool) path: figure/title
#: ../help/C/general.page:18
msgid "Example of metadata in a picture file"
msgstr "Esempio di metadati in un file immagine"

#. (itstool) path: figure/desc
#: ../help/C/general.page:20
msgid ""
"The file discloses information about the hardware, settings, date, location "
"of the shoot. It contains a total of 79 metadata."
msgstr ""
"Il file fornisce informazioni sull'hardware, le impostazioni, la data, il "
"luogo delle riprese. Contiene un totale di 79 metadati."

#. (itstool) path: section/title
#: ../help/C/general.page:27
msgid "Cleaning process"
msgstr "Processo di pulizia"

#. (itstool) path: section/p
#: ../help/C/general.page:28
msgid ""
"While <app>Metadata Cleaner</app> is doing its very best to display "
"metadata, it doesn't mean that a file is clean from metadata if it doesn't "
"show any. There is no reliable way to detect every single possible metadata "
"for complex file formats. This is why you shouldn't rely on metadata's "
"presence to decide if your file must be cleaned or not."
msgstr ""
"Sebbene <app>Metadata Cleaner</app> sta facendo del suo meglio per "
"visualizzare i metadati, ciò non significa che un file sia pulito dai "
"metadati se non ne mostra alcuno. Non esiste un modo affidabile per rilevare "
"ogni singolo metadato possibile per formati di file complessi. Questo è il "
"motivo per cui non dovresti fare affidamento sulla presenza dei metadati per "
"decidere se il tuo file deve essere pulito o meno."

#. (itstool) path: section/p
#: ../help/C/general.page:29
msgid ""
"<app>Metadata Cleaner</app> takes the content of the file and puts it into a "
"new one without metadata, ensuring that any undetected metadata is stripped."
msgstr ""
"<app>Metadata Cleaner</app> prende il contenuto del file e lo inserisce in "
"uno nuovo senza metadati, assicurando che tutti i metadati non rilevati "
"vengano rimossi."

#. (itstool) path: section/title
#: ../help/C/general.page:35
msgid "Limitations"
msgstr "Limitazioni"

#. (itstool) path: section/p
#: ../help/C/general.page:36
msgid ""
"Be aware that metadata is not the only way of marking a file. If the content "
"itself discloses personal information or has been watermarked, traditionally "
"or via steganography, <app>Metadata Cleaner</app> will not protect you."
msgstr ""
"Tieni presente che i metadati non sono l'unico modo per contrassegnare un "
"file. Se il contenuto stesso rivela informazioni personali o è stato "
"filigranato, tradizionalmente o tramite steganografia,<app>Metadata Cleaner</"
"app> non ti proteggerà."

#. (itstool) path: page/title
#. The application name. It can be translated.
#: ../help/C/index.page:10
msgid "Metadata Cleaner"
msgstr "Pulitore di Metadati"

#. (itstool) path: page/p
#: ../help/C/index.page:14
msgid ""
"<app>Metadata Cleaner</app> allows you to view metadata in your files and to "
"get rid of it, as much as possible."
msgstr ""
"<app>Metadata Cleaner</app> ti consente di visualizzare i metadati nei tuoi "
"file e di sbarazzartene, per quanto possibile."

#. (itstool) path: section/title
#: ../help/C/index.page:17
msgid "General information"
msgstr "Informazioni generali"

#. (itstool) path: section/title
#: ../help/C/index.page:21
msgid "Using <app>Metadata Cleaner</app>"
msgstr "Usa <app>Metadata Cleaner</app>"

#. (itstool) path: section/title
#: ../help/C/usage.page:14
msgid "Adding files"
msgstr "Aggiungere file"

#. (itstool) path: section/p
#: ../help/C/usage.page:15
msgid ""
"To add files, press the <gui style=\"button\">Add Files</gui> button. A file "
"chooser will open, select the files you want to clean."
msgstr ""
"Per aggiungere file, premere il pulsante <gui style=\"button\">Aggiungi "
"file</gui> . Si aprirà un selettore di file, seleziona i file che vuoi "
"pulire."

#. (itstool) path: figure/title
#: ../help/C/usage.page:17
msgid "<gui style=\"button\">Add Files</gui> button"
msgstr "<gui style=\"button\">pulsante</gui> aggiungi file"

#. (itstool) path: section/p
#: ../help/C/usage.page:20
msgid ""
"To add whole folders at once, press the <gui style=\"button\">Add Folders</"
"gui> button. A file chooser will open, select the folders you want to add. "
"You can optionally choose to also add all files from all subfolders by "
"checking the <gui style=\"checkbox\">Add files from subfolders</gui> "
"checkbox."
msgstr ""
"Per aggiungere intere cartelle in una volta, premi il pulsante <gui style="
"\"button\">Aggiungi cartelle</gui>. Si aprirà un selettore di file, "
"selezionare le cartelle che si desidera aggiungere. Puoi facoltativamente "
"scegliere di aggiungere anche tutti i file da tutte le sottocartelle "
"selezionando la casella <gui style=\"checkbox\">Aggiungi file dalle "
"sottocartelle</gui>."

#. (itstool) path: figure/title
#: ../help/C/usage.page:22
msgid "<gui style=\"button\">Add Folders</gui> button"
msgstr "Pulsante <gui style=\"button\">Aggiungi cartelle</gui>"

#. (itstool) path: section/title
#: ../help/C/usage.page:30
msgid "Viewing metadata"
msgstr "Visualizzazione dei metadati"

#. (itstool) path: section/p
#: ../help/C/usage.page:31
msgid ""
"Click on a file in the list to open the detailed view. If it has metadata, "
"it will be shown there."
msgstr ""
"Fare clic su un file nell'elenco per aprire la visualizzazione dettagliata. "
"Se ha metadati, verrà mostrato lì."

#. (itstool) path: figure/title
#: ../help/C/usage.page:33
msgid "Detailed view of the metadata"
msgstr "Visualizzazione dettagliata dei metadati"

#. (itstool) path: section/title
#: ../help/C/usage.page:41
msgid "Cleaning files"
msgstr "Pulizia dei file"

#. (itstool) path: section/p
#: ../help/C/usage.page:42
msgid ""
"To clean all the files in the window, press the <gui style=\"button\">Clean</"
"gui> button. The cleaning process may take some time to complete."
msgstr ""
"Per pulire tutti i file nella finestra, premere il <gui style=\"button"
"\">Pulsante</gui> pulisci. Il completamento del processo di pulizia potrebbe "
"richiedere del tempo."

#. (itstool) path: figure/title
#: ../help/C/usage.page:44
msgid "<gui style=\"button\">Clean</gui> button"
msgstr "<gui style=\"button\">Pulsante</gui> pulisci"

#. (itstool) path: section/title
#: ../help/C/usage.page:52
msgid "Lightweight cleaning"
msgstr "Modalità leggera"

#. (itstool) path: section/p
#: ../help/C/usage.page:53
msgid ""
"By default, the removal process might alter a bit the data of your files, in "
"order to remove as much metadata as possible. For example, texts in PDF "
"might not be selectable anymore, compressed images might get compressed "
"again…"
msgstr ""
"Per impostazione predefinita, il processo di rimozione potrebbe alterare "
"leggermente i dati dei tuoi file, al fine di rimuovere quanti più metadati "
"possibile. Ad esempio, i testi in PDF potrebbero non essere più "
"selezionabili, le immagini compresse potrebbero essere compresse di nuovo…"

#. (itstool) path: section/p
#: ../help/C/usage.page:54
msgid ""
"The lightweight mode, while not as thorough, will not make destructive "
"changes to your files."
msgstr ""
"La modalità leggera, sebbene non così completa, non apporterà modifiche "
"distruttive ai tuoi file."

#~ msgid ""
#~ "If you're willing to trade some metadata's presence in exchange of the "
#~ "guarantee that the data of your files won't be modified, the lightweight "
#~ "mode precisely does that."
#~ msgstr ""
#~ "Se sei disposto a scambiare la presenza di alcuni metadati in cambio "
#~ "della garanzia che i dati dei tuoi file non verranno modificati, la "
#~ "modalità leggera fa proprio questo."
