# Victor Mihalache <vmhl.ph@gmail.com>, 2021.
# Andrea Andre <andrea.tsg19@slmail.me>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-09-11 18:41+0000\n"
"PO-Revision-Date: 2023-11-15 14:05+0000\n"
"Last-Translator: Andrea Andre <andrea.tsg19@slmail.me>\n"
"Language-Team: Romanian <https://hosted.weblate.org/projects/"
"metadata-cleaner/help/ro/>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Weblate 5.2-dev\n"

#. (itstool) path: section/title
#: ../help/C/general.page:14
msgid "Metadata and privacy"
msgstr "Metadate și intimitate"

#. (itstool) path: section/p
#: ../help/C/general.page:15
msgid ""
"Metadata consists of information that characterizes data. Metadata is used "
"to provide documentation for data products. In essence, metadata answers "
"who, what, when, where, why, and how about every facet of the data that is "
"being documented."
msgstr ""
"Metadatele constau în informații care caracterizează datele. Metadatele sunt "
"utilizate pentru a furniza documentația pentru produsele de date. În esență, "
"metadatele răspund la cine, ce, când, unde, de ce și cum despre fiecare "
"aspect al datelor care sunt documentate."

#. (itstool) path: section/p
#: ../help/C/general.page:16
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"Metadatele dintr-un fișier pot spune multe despre tine. Aparatele foto "
"înregistrează date despre momentul și locul în care a fost făcută o "
"fotografie și despre aparatul foto utilizat. Aplicațiile Office adaugă "
"automat informații despre autor și companie în documente și foi de calcul. "
"Acestea sunt informații sensibile și este posibil să nu dorești să le "
"dezvălui."

#. (itstool) path: figure/title
#: ../help/C/general.page:18
msgid "Example of metadata in a picture file"
msgstr "Exemplu de metadate într-un fișier imagine"

#. (itstool) path: figure/desc
#: ../help/C/general.page:20
msgid ""
"The file discloses information about the hardware, settings, date, location "
"of the shoot. It contains a total of 79 metadata."
msgstr ""
"Fișierul dezvăluie informații despre aparat, reglări, data și locul unde a "
"avut loc filmarea. Conține un total de 79 de metadate."

#. (itstool) path: section/title
#: ../help/C/general.page:27
msgid "Cleaning process"
msgstr "Procesul de curățare"

#. (itstool) path: section/p
#: ../help/C/general.page:28
msgid ""
"While <app>Metadata Cleaner</app> is doing its very best to display "
"metadata, it doesn't mean that a file is clean from metadata if it doesn't "
"show any. There is no reliable way to detect every single possible metadata "
"for complex file formats. This is why you shouldn't rely on metadata's "
"presence to decide if your file must be cleaned or not."
msgstr ""
"În timp ce <app>Curățător de Metadate</app> face tot posibilul să afișeze "
"metadatele, nu înseamnă că un fișier este curat de metadate dacă nu afișează "
"niciuna. Nu există nicio modalitate fiabilă de a detecta toate metadatele "
"posibile pentru formate de fișiere complexe. De aceea, nu ar trebui să te "
"bazezi pe prezența metadatelor pentru a decide dacă fișierul tău trebuie "
"curățat sau nu."

#. (itstool) path: section/p
#: ../help/C/general.page:29
msgid ""
"<app>Metadata Cleaner</app> takes the content of the file and puts it into a "
"new one without metadata, ensuring that any undetected metadata is stripped."
msgstr ""
"<app>Curățător de Metadate</app> preia conținutul fișierului și îl introduce "
"într-unul nou, fără metadate, asigurându-se că orice metadate nedetectate "
"sunt eliminate."

#. (itstool) path: section/title
#: ../help/C/general.page:35
msgid "Limitations"
msgstr "Limitări"

#. (itstool) path: section/p
#: ../help/C/general.page:36
msgid ""
"Be aware that metadata is not the only way of marking a file. If the content "
"itself discloses personal information or has been watermarked, traditionally "
"or via steganography, <app>Metadata Cleaner</app> will not protect you."
msgstr ""
"Reține că metadatele nu reprezintă singura modalitate de marcare a unui "
"fișier. În cazul în care conținutul în sine dezvăluie informații personale "
"sau a fost filigranat, în mod tradițional sau prin steganografie, "
"<app>Curățător de Metadate</app> nu te va proteja."

#. (itstool) path: page/title
#. The application name. It can be translated.
#: ../help/C/index.page:10
msgid "Metadata Cleaner"
msgstr "Curățător de Metadate"

#. (itstool) path: page/p
#: ../help/C/index.page:14
msgid ""
"<app>Metadata Cleaner</app> allows you to view metadata in your files and to "
"get rid of it, as much as possible."
msgstr ""
"<app>Curățător de Metadate</app> îți permite vizualizarea metadatelor din "
"fișierele tale și să scapi de ele, pe cât posibil."

#. (itstool) path: section/title
#: ../help/C/index.page:17
msgid "General information"
msgstr "Informații generale"

#. (itstool) path: section/title
#: ../help/C/index.page:21
msgid "Using <app>Metadata Cleaner</app>"
msgstr "Folosirea <app>Curățător de Metadate</app>"

#. (itstool) path: section/title
#: ../help/C/usage.page:14
msgid "Adding files"
msgstr "Adăugare de fișiere"

#. (itstool) path: section/p
#: ../help/C/usage.page:15
msgid ""
"To add files, press the <gui style=\"button\">Add Files</gui> button. A file "
"chooser will open, select the files you want to clean."
msgstr ""
"Pentru a adăuga fișiere, apasă butonul <gui style=\"button\">Adaugă fișiere</"
"gui>. Se va deschide un selector de fișiere; alege fișierele pe care vrei să "
"le cureți."

#. (itstool) path: figure/title
#: ../help/C/usage.page:17
msgid "<gui style=\"button\">Add Files</gui> button"
msgstr "Butonul <gui style=\"button\">Adaugă fișiere</gui>"

#. (itstool) path: section/p
#: ../help/C/usage.page:20
msgid ""
"To add whole folders at once, press the <gui style=\"button\">Add Folders</"
"gui> button. A file chooser will open, select the folders you want to add. "
"You can optionally choose to also add all files from all subfolders by "
"checking the <gui style=\"checkbox\">Add files from subfolders</gui> "
"checkbox."
msgstr ""
"Pentru a adăuga dosare întregi deodată, apasă butonul <gui style=\"button\""
">Adaugă dosare</gui>. Se va deschide un selector de fișiere; alege dosarele "
"pe care vrei să le adaugi. Opțional, poți alege să adaugi și toate fișierele "
"din toate subdosarele, bifând <gui style=\"checkbox\">Adaugă fișiere din "
"subdosare</gui>."

#. (itstool) path: figure/title
#: ../help/C/usage.page:22
msgid "<gui style=\"button\">Add Folders</gui> button"
msgstr "Butonul <gui style=\"button\">Adaugă dosare</gui>"

#. (itstool) path: section/title
#: ../help/C/usage.page:30
msgid "Viewing metadata"
msgstr "Vizualizarea metadatelor"

#. (itstool) path: section/p
#: ../help/C/usage.page:31
msgid ""
"Click on a file in the list to open the detailed view. If it has metadata, "
"it will be shown there."
msgstr ""
"Apasă pe un fișier din listă pentru a deschide vizualizarea detaliată. Dacă "
"acesta are metadate, acestea vor fi afișate acolo."

#. (itstool) path: figure/title
#: ../help/C/usage.page:33
msgid "Detailed view of the metadata"
msgstr "Vizualizare detaliată a metadatelor"

#. (itstool) path: section/title
#: ../help/C/usage.page:41
msgid "Cleaning files"
msgstr "Curățarea fișierelor"

#. (itstool) path: section/p
#: ../help/C/usage.page:42
msgid ""
"To clean all the files in the window, press the <gui style=\"button\">Clean</"
"gui> button. The cleaning process may take some time to complete."
msgstr ""
"Pentru a curăța toate fișierele din fereastră, apasă butonul <gui style="
"\"button\">Curăță</gui>. Este posibil ca procesul de curățare să dureze ceva "
"timp."

#. (itstool) path: figure/title
#: ../help/C/usage.page:44
msgid "<gui style=\"button\">Clean</gui> button"
msgstr "Butonul <gui style=\"button\">Curăță</gui>"

#. (itstool) path: section/title
#: ../help/C/usage.page:52
msgid "Lightweight cleaning"
msgstr "Curățare ușoară"

#. (itstool) path: section/p
#: ../help/C/usage.page:53
msgid ""
"By default, the removal process might alter a bit the data of your files, in "
"order to remove as much metadata as possible. For example, texts in PDF "
"might not be selectable anymore, compressed images might get compressed "
"again…"
msgstr ""
"În mod prestabilit, procesul de eliminare ar putea modifica puțin datele "
"fișierelor tale, pentru a elimina cât mai multe metadate posibil. De "
"exemplu, este posibil ca textele din PDF să nu mai poată fi selecționabile, "
"iar imaginile comprimate să fie din nou comprimate…"

#. (itstool) path: section/p
#: ../help/C/usage.page:54
msgid ""
"The lightweight mode, while not as thorough, will not make destructive "
"changes to your files."
msgstr ""
"Modul ușor, deși nu este la fel de riguros, nu va efectua modificări "
"distructive ale fișierelor tău."
